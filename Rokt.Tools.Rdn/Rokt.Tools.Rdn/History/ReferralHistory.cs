﻿using Newtonsoft.Json;
using System;

namespace Rokt.Tools.Rdn.History
{
	public class ReferralHistory
	{
		public string SessionId { get; set; }

		public DateTime GeneratedAt { get; set; }

		[JsonProperty(PropertyName = "accountId")]
		public long AdvertiserId { get; set; }

		public long CampaignId { get; set; }
	}
}

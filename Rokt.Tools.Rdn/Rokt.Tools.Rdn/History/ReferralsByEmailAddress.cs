﻿namespace Rokt.Tools.Rdn.History
{
	public class ReferralsByEmailAddress
	{
		public string EmailHash { get; set; }
		public ReferralHistory[] ReferralHistory { get; set; }
	}
}

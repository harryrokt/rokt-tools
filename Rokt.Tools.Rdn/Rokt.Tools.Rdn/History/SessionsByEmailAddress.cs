﻿namespace Rokt.Tools.Rdn.History
{
	public class SessionsByEmailAddress
	{
		public string EmailHash { get; set; }

		public SessionHistory[] Sessions { get; set; }
	}
}

﻿using Newtonsoft.Json;
using System;

namespace Rokt.Tools.Rdn.History
{
	public class SessionHistory
	{
		public string SessionId { get; set; }

		public DateTime GeneratedAt { get; set; }

		[JsonProperty(PropertyName = "accountId")]
		public long PartnerId { get; set; }
	}
}

﻿using Flurl;
using Flurl.Http;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Rokt.Tools.Rdn.History
{
	public class RdnHistoryService
	{
		private readonly string _rdnServiceUrl;

		public RdnHistoryService(string rdnServiceUrl)
		{
			_rdnServiceUrl = rdnServiceUrl ?? throw new ArgumentNullException(nameof(rdnServiceUrl));
		}

		public async Task<SessionsByEmailAddress> GetSessionsByEmailAddress(string emailAddress)
		{
			var hash = ComputeBase64Sha256Hash(emailAddress);

			var daResponse = await _rdnServiceUrl
				.AppendPathSegments("rdn-history", "sessions")
				.SetQueryParam("emailHash", hash)
				.GetAsync()
				.ReceiveJson<SessionsByEmailAddress>();

			return daResponse;
		}

		public async Task<ReferralsByEmailAddress> GetReferralsByEmailAddress(string emailAddress)
		{
			var hash = ComputeBase64Sha256Hash(emailAddress);

			var daResponse = await _rdnServiceUrl
				.AppendPathSegments("rdn-history", "referrals")
				.SetQueryParam("emailHash", hash)
				.GetAsync()
				.ReceiveJson<ReferralsByEmailAddress>();

			return daResponse;
		}

		private string ComputeBase64Sha256Hash(string input)
		{
			input ??= string.Empty;
			using var sha256 = System.Security.Cryptography.SHA256.Create();
			var sha256Bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));

			return Convert.ToBase64String(sha256Bytes);
		}
	}
}

﻿using CommandLine;
using Newtonsoft.Json;
using Rokt.Tools.Rdn.History;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rokt.Tools.Rdn
{
	class Program
	{
		private static readonly Dictionary<Environment, string> _rdnServiceUrls = new Dictionary<Environment, string>
		{
			[Environment.Build] = "https://da-ecs-us-west-2.build.roktinternal.com",
			[Environment.Stage] = "https://da-ecs-us-west-2.stage.roktinternal.com",
			[Environment.Prod] = "https://da-ecs-us-west-2.roktinternal.com"
		};

		public static async Task Main(string[] args)
		{
			var result = Parser.Default.ParseArguments<GetHistoryOptions, Dummy>(args);

			try
			{
				await result.MapResult(
					o => o switch
					{
						GetHistoryOptions ghOpts => ghOpts.ObjectType switch
						{
							"sessions" => PrintSessionsByEmailAddress(ghOpts.EmailAddress, ghOpts.Environment),
							"referrals" => PrintReferralsByEmailAddress(ghOpts.EmailAddress, ghOpts.Environment),
							_ => throw new NotSupportedException($"Object type {ghOpts.ObjectType} is not supported."),
						},
						_ => throw new NotSupportedException($"Verb {args[0]} is not supported."),
					},
					err => Task.CompletedTask
				);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		private static async Task PrintSessionsByEmailAddress(string emailAddress, Environment environment)
		{
			if (string.IsNullOrEmpty(emailAddress))
			{
				throw new ArgumentNullException("Email address");
			}

			var rdnServiceUrl = _rdnServiceUrls[environment];
			var service = new RdnHistoryService(rdnServiceUrl);

			var response = await service.GetSessionsByEmailAddress(emailAddress);

			Console.WriteLine(JsonConvert.SerializeObject(response, Formatting.Indented));
		}

		private static async Task PrintReferralsByEmailAddress(string emailAddress, Environment environment)
		{
			if (string.IsNullOrEmpty(emailAddress))
			{
				throw new ArgumentNullException("Email address");
			}

			var rdnServiceUrl = _rdnServiceUrls[environment];
			var service = new RdnHistoryService(rdnServiceUrl);

			var response = await service.GetReferralsByEmailAddress(emailAddress);

			Console.WriteLine(JsonConvert.SerializeObject(response, Formatting.Indented));
		}

		[Verb("get-history", HelpText = "Get RDN history of an object.")]
		public class GetHistoryOptions
		{
			[Value(1)]
			public string ObjectType { get; set; }

			[Option('e', "email")]
			public string EmailAddress { get; set; }

			[Option('p', "env")]
			public string Env { get; set; }

			public Environment Environment => Env switch
			{
				"build" => Environment.Build,
				"stage" => Environment.Stage,
				"prod" => Environment.Prod,
				_ => throw new ArgumentOutOfRangeException($"Environment {Env} is not supported."),
			};
		}

		[Verb(".")]
		public class Dummy
		{
		}

		public enum Environment
		{
			Build = 1,
			Stage,
			Prod
		}
	}
}

## Description

This repository contains many dotnet tools to assist developing locally.

They require dotnet runtime 3.0 on your machine to run.

## Installation

Get into each tool directory, run `./publish-locally.sh`

## Tool list

### rdn

** Command: ** `rdn`
** Directory: ** `Rokt.Tools.Rdn`

#### Description
`rdn` is used to query many RDN APIs mentioned [here](https://rokton.atlassian.net/wiki/spaces/DA/pages/598706445/RDN+history+API)

#### Usage

##### Get History

###### Get all sessions associated to an email address

`rdn get-history sessions --email harry.ninh@rokt.com --env stage`